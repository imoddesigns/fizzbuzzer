﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzLib;

namespace ConsoleApplicationPCCA
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintFizzBuzz();
        }



        private static void PrintFizzBuzz()
        {

            var fb = new FizzBuzzer();

            fb.FizzBuzz(1, 100, new Dictionary<int, string>()
            {
                {3, "Foo" },
                {5, "Bar" },
                {7, "Bazz" },
                {11, "Banana" }
            }, s =>
          {
              Console.WriteLine(s);
          });


            Console.Read();
        }
    }
}
