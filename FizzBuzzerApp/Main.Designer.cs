﻿namespace FizzBuzzerApp
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputWindow = new FizzBuzzerApp.Views.OutputWindow();
            this.multiplesInput1 = new FizzBuzzerApp.Views.MultiplesInput();
            this.rangeInput1 = new FizzBuzzerApp.Views.RangeInput();
            this.SuspendLayout();
            // 
            // outputWindow
            // 
            this.outputWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputWindow.Location = new System.Drawing.Point(437, 47);
            this.outputWindow.Name = "outputWindow";
            this.outputWindow.Size = new System.Drawing.Size(560, 561);
            this.outputWindow.TabIndex = 5;
            // 
            // multiplesInput1
            // 
            this.multiplesInput1.Location = new System.Drawing.Point(12, 140);
            this.multiplesInput1.Name = "multiplesInput1";
            this.multiplesInput1.Size = new System.Drawing.Size(418, 468);
            this.multiplesInput1.TabIndex = 4;
            // 
            // rangeInput1
            // 
            this.rangeInput1.Location = new System.Drawing.Point(12, 22);
            this.rangeInput1.Name = "rangeInput1";
            this.rangeInput1.Size = new System.Drawing.Size(418, 97);
            this.rangeInput1.TabIndex = 0;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 630);
            this.Controls.Add(this.outputWindow);
            this.Controls.Add(this.multiplesInput1);
            this.Controls.Add(this.rangeInput1);
            this.Name = "Main";
            this.Text = "FizzBuzzer";
            this.ResumeLayout(false);

        }

        #endregion

        private Views.RangeInput rangeInput1;
        private Views.MultiplesInput multiplesInput1;
        private Views.OutputWindow outputWindow;
    }
}

