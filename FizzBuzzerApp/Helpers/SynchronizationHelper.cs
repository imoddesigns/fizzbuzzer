﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FizzBuzzerApp.Helpers
{
    public static class SynchronizationHelper
    {

        public static void Invoke(Action action)
        {
            var uiContext = WindowsFormsSynchronizationContext.Current;

            if (uiContext == null)
            {
                WindowsFormsSynchronizationContext.SetSynchronizationContext(
                             new WindowsFormsSynchronizationContext());

                uiContext = WindowsFormsSynchronizationContext.Current;

                Send(action, uiContext);
            }
        }


        private static void Send(Action action, SynchronizationContext uiContext)
        {
            uiContext.Send(new SendOrPostCallback(
           delegate (object state)
           {
               action();
           }
           ), null);
        }
    }
}
