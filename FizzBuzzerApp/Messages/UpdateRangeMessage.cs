﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzerApp.Messages
{
    public class UpdateRangeMessage
    {
        public int StartRange { get; set; }
        public int EndRange { get; set; }
    }
}
