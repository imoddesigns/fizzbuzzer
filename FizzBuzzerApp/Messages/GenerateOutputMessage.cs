﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzerApp.Messages
{
    public class GenerateOutputMessage
    {
        public Dictionary<int, string> MultiplesSet { get; set; }
    }
}
