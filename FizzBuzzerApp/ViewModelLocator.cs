﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzerApp.Contracts;
using FizzBuzzerApp.Services;
using FizzBuzzerApp.ViewModels;
using TinyIoC;

namespace FizzBuzzerApp
{
    public class ViewModelLocator
    {
        
        static ViewModelLocator()
        {
            TinyIoCContainer.Current.Register<IFizzBuzzerService, FizzerBuzzerDialog>();
            TinyIoCContainer.Current.Register<IMessageBus, MessageBus>().AsSingleton();
            TinyIoCContainer.Current.Register<IMessageService, MessageService>();

            TinyIoCContainer.Current.Register<MainViewModel>();
            TinyIoCContainer.Current.Register<MultipleInputViewModel>();
            TinyIoCContainer.Current.Register<RangeInputViewModel>();
        }

        public static MainViewModel Main
        {
            get
            {
                return TinyIoCContainer.Current.Resolve<MainViewModel>(); 
                
            }
        }


        public static RangeInputViewModel RangeInput
        {
            get
            {
                return TinyIoCContainer.Current.Resolve<RangeInputViewModel>();
            }
        }

        public static MultipleInputViewModel MultipleInput
        {
            get
            {
                return TinyIoCContainer.Current.Resolve<MultipleInputViewModel>();
            }
        }
    }
}
