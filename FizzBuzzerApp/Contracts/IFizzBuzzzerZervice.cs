﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzerApp.Contracts
{
    public interface IFizzBuzzerService
    {

        void Generate(int startRange, int endRange, Dictionary<int, string> sets, Action<string> onOutputGenerated);
        void Dispose();
    }
}
