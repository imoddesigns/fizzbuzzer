﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzerApp.Contracts
{
    public interface IMessageService
    {
        void DisplayError(string title, string messages);
    }
}
