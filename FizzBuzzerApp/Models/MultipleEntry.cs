﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzerApp.Models
{
    public class MultipleEntry
    {
        public string Multiple { get; set; }
        public string Value { get; set; }
    }
}
