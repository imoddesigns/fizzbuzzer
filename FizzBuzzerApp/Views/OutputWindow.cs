﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FizzBuzzerApp.ViewModels;

namespace FizzBuzzerApp.Views
{
    public partial class OutputWindow : UserControl
    {
        private readonly MainViewModel _viewModel;

        public OutputWindow()
        {
            InitializeComponent();
            _viewModel = ViewModelLocator.Main;

            outputTextBox.DataBindings.Add("Text", _viewModel, nameof(_viewModel.GeneratedOutput), false,
                DataSourceUpdateMode.OnPropertyChanged);

            currentRangeLabel.DataBindings.Add("Text", _viewModel, nameof(_viewModel.CurrentRange), false,
                DataSourceUpdateMode.OnPropertyChanged);

        }

        private void ClearButtonClick(object sender, EventArgs e)
        {
           _viewModel.ClearOutput();
        }

    
    }
}
