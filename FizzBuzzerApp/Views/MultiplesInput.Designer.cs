﻿namespace FizzBuzzerApp.Views
{
    partial class MultiplesInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.generateButton = new System.Windows.Forms.Button();
            this.multiplesDataGridView = new System.Windows.Forms.DataGridView();
            this.Multiple = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inputTwo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.multipleTextBox = new System.Windows.Forms.TextBox();
            this.actionButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.multiplesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // outputTextBox
            // 
            this.outputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputTextBox.Location = new System.Drawing.Point(199, 23);
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.Size = new System.Drawing.Size(190, 22);
            this.outputTextBox.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.generateButton, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.multiplesDataGridView, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.outputTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.inputTwo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.multipleTextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.actionButton, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(392, 465);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // generateButton
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.generateButton, 2);
            this.generateButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generateButton.Location = new System.Drawing.Point(3, 391);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(386, 71);
            this.generateButton.TabIndex = 7;
            this.generateButton.Text = "Generate";
            this.generateButton.UseVisualStyleBackColor = true;
            // 
            // multiplesDataGridView
            // 
            this.multiplesDataGridView.AllowUserToAddRows = false;
            this.multiplesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.multiplesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Multiple,
            this.Value});
            this.tableLayoutPanel1.SetColumnSpan(this.multiplesDataGridView, 2);
            this.multiplesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multiplesDataGridView.Location = new System.Drawing.Point(3, 91);
            this.multiplesDataGridView.Name = "multiplesDataGridView";
            this.multiplesDataGridView.RowHeadersWidth = 20;
            this.multiplesDataGridView.RowTemplate.Height = 24;
            this.multiplesDataGridView.ShowCellToolTips = false;
            this.multiplesDataGridView.Size = new System.Drawing.Size(386, 294);
            this.multiplesDataGridView.TabIndex = 5;
            // 
            // Multiple
            // 
            this.Multiple.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Multiple.FillWeight = 101.5228F;
            this.Multiple.HeaderText = "Multiple";
            this.Multiple.Name = "Multiple";
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.FillWeight = 98.47716F;
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            // 
            // inputTwo
            // 
            this.inputTwo.AutoSize = true;
            this.inputTwo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputTwo.Location = new System.Drawing.Point(199, 0);
            this.inputTwo.Name = "inputTwo";
            this.inputTwo.Size = new System.Drawing.Size(190, 20);
            this.inputTwo.TabIndex = 1;
            this.inputTwo.Text = "output:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "multiple:";
            // 
            // multipleTextBox
            // 
            this.multipleTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multipleTextBox.Location = new System.Drawing.Point(3, 23);
            this.multipleTextBox.Name = "multipleTextBox";
            this.multipleTextBox.Size = new System.Drawing.Size(190, 22);
            this.multipleTextBox.TabIndex = 2;
            // 
            // actionButton
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.actionButton, 2);
            this.actionButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.actionButton.Location = new System.Drawing.Point(3, 51);
            this.actionButton.Name = "actionButton";
            this.actionButton.Size = new System.Drawing.Size(386, 34);
            this.actionButton.TabIndex = 4;
            this.actionButton.Text = "Add Multiple";
            this.actionButton.UseVisualStyleBackColor = true;
            // 
            // MultiplesInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MultiplesInput";
            this.Size = new System.Drawing.Size(392, 465);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.multiplesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox outputTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label inputTwo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox multipleTextBox;
        private System.Windows.Forms.Button actionButton;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.DataGridView multiplesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Multiple;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}
