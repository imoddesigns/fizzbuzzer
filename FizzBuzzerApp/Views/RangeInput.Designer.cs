﻿namespace FizzBuzzerApp.Views
{
    partial class RangeInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.endRangeTextBox = new System.Windows.Forms.TextBox();
            this.inputTwo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.startRangeTextBox = new System.Windows.Forms.TextBox();
            this.actionButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.endRangeTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.inputTwo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.startRangeTextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.actionButton, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(418, 97);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // endRangeTextBox
            // 
            this.endRangeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.endRangeTextBox.Location = new System.Drawing.Point(212, 23);
            this.endRangeTextBox.Name = "endRangeTextBox";
            this.endRangeTextBox.Size = new System.Drawing.Size(203, 22);
            this.endRangeTextBox.TabIndex = 3;
            // 
            // inputTwo
            // 
            this.inputTwo.AutoSize = true;
            this.inputTwo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputTwo.Location = new System.Drawing.Point(212, 0);
            this.inputTwo.Name = "inputTwo";
            this.inputTwo.Size = new System.Drawing.Size(203, 20);
            this.inputTwo.TabIndex = 1;
            this.inputTwo.Text = "range end:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "range start:";
            // 
            // startRangeTextBox
            // 
            this.startRangeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.startRangeTextBox.Location = new System.Drawing.Point(3, 23);
            this.startRangeTextBox.Name = "startRangeTextBox";
            this.startRangeTextBox.Size = new System.Drawing.Size(203, 22);
            this.startRangeTextBox.TabIndex = 2;
            // 
            // actionButton
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.actionButton, 2);
            this.actionButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.actionButton.Location = new System.Drawing.Point(3, 51);
            this.actionButton.Name = "actionButton";
            this.actionButton.Size = new System.Drawing.Size(412, 38);
            this.actionButton.TabIndex = 4;
            this.actionButton.Text = "Update Range";
            this.actionButton.UseVisualStyleBackColor = true;
            // 
            // RangeInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "RangeInput";
            this.Size = new System.Drawing.Size(418, 97);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox endRangeTextBox;
        private System.Windows.Forms.Label inputTwo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox startRangeTextBox;
        private System.Windows.Forms.Button actionButton;
    }
}
