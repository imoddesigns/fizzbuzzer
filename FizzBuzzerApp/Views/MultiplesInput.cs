﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FizzBuzzerApp.Extensions;
using FizzBuzzerApp.ViewModels;

namespace FizzBuzzerApp.Views
{
    public partial class MultiplesInput : UserControl
    {
        private readonly ErrorProvider _errorProvider;
        private ToolTip _errorTooltip;
        private Point _cellInError;
        private readonly MultipleInputViewModel _viewModel;

        public MultiplesInput()
        {
            InitializeComponent();

            _errorProvider = new ErrorProvider();

            _viewModel = ViewModelLocator.MultipleInput;

            multiplesDataGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            multiplesDataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            multiplesDataGridView.Columns[0].DataPropertyName = "Multiple";
            multiplesDataGridView.Columns[1].DataPropertyName = "Value";

            multiplesDataGridView.AutoGenerateColumns = false;
            multiplesDataGridView.DataSource = _viewModel.Multiples;

            multiplesDataGridView.CellPainting += MultiplesDataGridViewOnCellPainting;
            multiplesDataGridView.CellValidating += MultiplesDataGridViewOnCellValidating;
            multiplesDataGridView.CellEndEdit += MultiplesDataGridViewOnCellEndEdit;
            multiplesDataGridView.CellMouseMove += MultiplesDataGridViewOnCellMouseMove;
            multiplesDataGridView.CellMouseLeave += MultiplesDataGridViewOnCellMouseLeave;


            multipleTextBox.DataBindings.Add("Text", _viewModel, nameof(_viewModel.Multiple), false,
                DataSourceUpdateMode.OnPropertyChanged);

            outputTextBox.DataBindings.Add("Text", _viewModel, nameof(_viewModel.Value), false,
               DataSourceUpdateMode.OnPropertyChanged);

            actionButton.DataBindings.Add("Enabled", _viewModel, nameof(_viewModel.CanAddNewMultiple), false,
                DataSourceUpdateMode.OnPropertyChanged);

            generateButton.DataBindings.Add("Enabled", _viewModel, nameof(_viewModel.CanGenerateOutput), false,
                DataSourceUpdateMode.OnPropertyChanged);


            multipleTextBox.Validating += MultipleTextBoxOnValidating;
            outputTextBox.Validating += OutputTextBoxOnValidating;

            actionButton.Click += (sender, args) => _viewModel.InsertNew();
            generateButton.Click += (sender, args) => _viewModel.GenerateOutput(); ;
        }
        private void MultiplesDataGridViewOnCellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (_cellInError.X == e.ColumnIndex && _cellInError.Y == e.RowIndex)
            {
                if (_errorTooltip != null && _errorTooltip.Active)
                {
                    _errorTooltip.Hide(multiplesDataGridView);
                    _errorTooltip.Active = false;
                }
            }
        }

        private void MultiplesDataGridViewOnCellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (_cellInError.X == e.ColumnIndex && _cellInError.Y == e.RowIndex)
            {
                DataGridViewCell cell = multiplesDataGridView[e.ColumnIndex, e.RowIndex];

                if (cell.ErrorText != String.Empty)
                {
                    if (!_errorTooltip.Active)
                    {
                        _errorTooltip.Show(cell.ErrorText, multiplesDataGridView, 1000);
                    }
                    _errorTooltip.Active = true;
                }
            }
        }

        private void MultiplesDataGridViewOnCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (multiplesDataGridView[e.ColumnIndex, e.RowIndex].ErrorText != String.Empty)
            {
                DataGridViewCell cell = multiplesDataGridView[e.ColumnIndex, e.RowIndex];
                cell.ErrorText = String.Empty;
                _cellInError = new Point(-2, -2);

                // restore padding for cell. This moves the editing control
                cell.Style.Padding = (Padding)cell.Tag;

                // hide and dispose tooltip
                if (_errorTooltip != null)
                {
                    _errorTooltip.Hide(multiplesDataGridView);
                    _errorTooltip.Dispose();
                    _errorTooltip = null;
                }
            }
        }

        private void MultiplesDataGridViewOnCellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (multiplesDataGridView.IsCurrentCellDirty && !String.IsNullOrEmpty(e.ErrorText))
            {
                // paint everything except error icon
                e.Paint(e.ClipBounds, DataGridViewPaintParts.All &
                                ~(DataGridViewPaintParts.ErrorIcon));

                // now move error icon over to fill in the padding space
                GraphicsContainer container = e.Graphics.BeginContainer();
                e.Graphics.TranslateTransform(18, 0);
                e.Paint(this.ClientRectangle, DataGridViewPaintParts.ErrorIcon);
                e.Graphics.EndContainer(container);

                e.Handled = true;
            }
        }

        private void OutputTextBoxOnValidating(object sender, CancelEventArgs e)
        {
            _viewModel.CanAddNewMultiple = this.HandleTextBoxValidation(_viewModel.ValidateValueInput,
                outputTextBox, _errorProvider, e);
        }

        private void MultipleTextBoxOnValidating(object sender, CancelEventArgs e)
        {
            _viewModel.CanAddNewMultiple = this.HandleTextBoxValidation(_viewModel.ValidateMultipleInput,
                multipleTextBox, _errorProvider, e);
        }

        private void MultiplesDataGridViewOnCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DataGridViewColumn column = multiplesDataGridView.Columns[e.ColumnIndex];

            if (column.Name == "Multiple")
            {
                string error = _viewModel.ValidateMultipleInput(e.FormattedValue.ToString());

                if (!string.IsNullOrEmpty(error))
                {
                    e.Cancel = true;
                    AnnotateCell(error, e.RowIndex, e.ColumnIndex);
                }
                else
                {
                    AnnotateCell(string.Empty, e.RowIndex, e.ColumnIndex);
                }



                //handle duplicate multiple values
                if (string.IsNullOrEmpty(error))
                {
                    bool duplicateFound = false;

                    for (int i = 0; i < multiplesDataGridView.RowCount; i++)
                    {
                        if (i == e.RowIndex)
                            continue;


                        DataGridViewCell cell = multiplesDataGridView.Rows[i].Cells[e.ColumnIndex];


                        if (cell.Value.ToString() == e.FormattedValue.ToString())
                        {
                            duplicateFound = true;
                            break;
                        }
                    }

                    if (duplicateFound)
                    {
                        AnnotateCell("duplicate inputs are not allowed", e.RowIndex, e.ColumnIndex);
                        e.Cancel = true;
                        _viewModel.MultiplesContainDuplicates = true;

                    }
                    else
                    {
                        AnnotateCell(string.Empty, e.RowIndex, e.ColumnIndex);
                        _viewModel.MultiplesContainDuplicates = false;
                    }
                }

            }
            else if (column.Name == "Value")
            {
                string error = _viewModel.ValidateValueInput(e.FormattedValue.ToString());

                if (!string.IsNullOrEmpty(error))
                {
                    e.Cancel = true;
                    AnnotateCell(error, e.RowIndex, e.ColumnIndex);
                }
                else
                {
                    AnnotateCell(string.Empty, e.RowIndex, e.ColumnIndex);
                }
            }




        }

        private void AnnotateCell(string errorMessage, int rowIndex, int colIndex)
        {

            DataGridViewCell cell = multiplesDataGridView.Rows[rowIndex].Cells[colIndex];
            cell.ErrorText = errorMessage;

            // increase padding for icon. This moves the editing control
            if (cell.Tag == null)
            {
                cell.Tag = cell.Style.Padding;
                cell.Style.Padding = new Padding(0, 0, 18, 0);
                _cellInError = new Point(colIndex, rowIndex);
            }
            if (_errorTooltip == null)
            {
                _errorTooltip = new ToolTip
                {
                    InitialDelay = 0,
                    ReshowDelay = 0,
                    Active = false
                };
            }
        }
    }
}
