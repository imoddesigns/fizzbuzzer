﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FizzBuzzerApp.ViewModels;
using FizzBuzzerApp.Extensions;


namespace FizzBuzzerApp.Views
{
    public partial class RangeInput : UserControl
    {
        private readonly ErrorProvider _errorProvider;
        private readonly RangeInputViewModel _viewModel;

        public RangeInput()
        {
            InitializeComponent();
            _errorProvider = new ErrorProvider(this);

            _viewModel = ViewModelLocator.RangeInput;

            startRangeTextBox.DataBindings.Add("Text", _viewModel, nameof(_viewModel.StartRange), false,
                DataSourceUpdateMode.OnPropertyChanged);

            endRangeTextBox.DataBindings.Add("Text", _viewModel, nameof(_viewModel.EndRange), false,
               DataSourceUpdateMode.OnPropertyChanged);

            actionButton.DataBindings.Add("Enabled", _viewModel, nameof(_viewModel.RangeIsValid), false,
                DataSourceUpdateMode.OnPropertyChanged);


            startRangeTextBox.Validating += StartRangeTextBoxOnValidating;
            endRangeTextBox.Validating += EndRangeTextBoxOnValidating;

            actionButton.Click += (sender, args) => _viewModel.UpdateRange();


        }


        private void StartRangeTextBoxOnValidating(object sender, CancelEventArgs e)
        {
            _viewModel.RangeIsValid =
                this.HandleTextBoxValidation(_viewModel.ValidateStartRange, startRangeTextBox, _errorProvider, e);
        }

        private void EndRangeTextBoxOnValidating(object sender, CancelEventArgs e)
        {
            _viewModel.RangeIsValid =
                this.HandleTextBoxValidation(_viewModel.ValidateEndRange, endRangeTextBox, _errorProvider, e);
        }

        
    }
}
