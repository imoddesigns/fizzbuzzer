﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FizzBuzzerApp.Contracts;
using FizzBuzzLib;

namespace FizzBuzzerApp.Services
{
    public partial class FizzerBuzzerDialog : Form, IFizzBuzzerService
    {

        private Action<string> _generateOputput;
        private CancellationTokenSource _cts;
        private FizzBuzzer _fizzBuzzer;


        public FizzerBuzzerDialog()
        {
            InitializeComponent();
           
            
        }


        public void Generate(int startRange, int endRange, Dictionary<int, string> sets, Action<string> onOutputGenerated)
        {
            _generateOputput = onOutputGenerated;

            string output = string.Empty;

            _cts = new CancellationTokenSource();
            _fizzBuzzer = new FizzBuzzer();
           
            cancelButton.Click  += CancelButtonOnClick;
            
            
            IProgress<int> progress = new Progress<int>(i =>
            {
                progressBar1.Value = i;

            });

            var fizzBuzzerTask = Task.Run(() =>
            {
              

                int current = startRange;

                

                _fizzBuzzer.FizzBuzz(startRange, endRange, sets, s =>
                {
                    int percent = (int)((float)(current * 100) / endRange);
                    progress.Report(percent);
                    output += s + Environment.NewLine;
                    current += 1;

                    if (_cts.IsCancellationRequested)
                    {
                        _fizzBuzzer.Cancel();
                        _fizzBuzzer = null;
                        _cts.Dispose();
                 
                    }
                });
            }, _cts.Token);

            fizzBuzzerTask.ContinueWith(a =>
            {

                _generateOputput(output);

                Invoke((MethodInvoker)delegate
                {
                    
                    this.Hide();
                    
                });

               
            });

            ShowDialog();
        }

        private void CancelButtonOnClick(object sender, EventArgs eventArgs)
        {
            if (_cts != null)
            {
                _cts.Cancel();
            }

            if(_fizzBuzzer != null)
                _fizzBuzzer.Cancel();

        }
    }
}
