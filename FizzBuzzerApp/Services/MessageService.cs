﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FizzBuzzerApp.Contracts;

namespace FizzBuzzerApp.Services
{
    public class MessageService:IMessageService
    {
        public void DisplayError(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
