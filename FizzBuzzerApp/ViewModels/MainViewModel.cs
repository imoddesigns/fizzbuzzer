﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FizzBuzzerApp.Contracts;
using FizzBuzzerApp.Helpers;
using FizzBuzzerApp.Messages;
using FizzBuzzerApp.Services;
using FizzBuzzLib;
using PropertyChanged;

namespace FizzBuzzerApp.ViewModels
{
    [ImplementPropertyChanged]
    public class MainViewModel
    {
        private readonly IFizzBuzzerService _fizzBuzzerService;

        private int _startRange = 1;
        private int _endRange = 100;

        public string CurrentRange { get; set; }

        private Dictionary<int, string> _multipleInputs;


        public bool CanGenerateOutput { get; set; }

        public string GeneratedOutput { get; set; }

        public MainViewModel(IMessageBus messageBus, IFizzBuzzerService fizzFizzBuzzerService)
        {

            _fizzBuzzerService = fizzFizzBuzzerService;

            messageBus.Subscribe<UpdateRangeMessage>(OnRangeUpdated);
            messageBus.Subscribe<GenerateOutputMessage>(OnGenerateOutput);
           

        }

        private void UpdateCurrentRange(int start, int end)
        {
            CurrentRange = string.Format("current range start: {0}, end: {1}", start, end);
        }

        public void OnRangeUpdated(UpdateRangeMessage message)
        {
            _startRange = message.StartRange;
            _endRange = message.EndRange;
            UpdateCurrentRange(_startRange, _endRange);
            
        }

        public void ClearOutput()
        {
            GeneratedOutput = string.Empty;
        }

        public void OnGenerateOutput(GenerateOutputMessage message)
        {
            ClearOutput();

            _multipleInputs = message.MultiplesSet;

            _fizzBuzzerService.Generate(_startRange, _endRange, _multipleInputs, OnWriteToOutput);
        }



        private void OnWriteToOutput(string s)
        {
            SynchronizationHelper.Invoke(() =>GeneratedOutput = s);
          
        }
    }
}