﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzerApp.Annotations;
using FizzBuzzerApp.Contracts;
using FizzBuzzerApp.Messages;
using FizzBuzzerApp.Models;
using PropertyChanged;

namespace FizzBuzzerApp.ViewModels
{
    [ImplementPropertyChanged]
    public class MultipleInputViewModel : INotifyPropertyChanged
    {
        private readonly IMessageService _messageService;
        private readonly IMessageBus _messageBus;


        public string Multiple { get; set; }
        public string Value { get; set; }


        public bool MultipleInputsValid { get; set; }
        public bool MultipleOutputsValid { get; set; }
        public bool MultiplesContainDuplicates { get; set; }

        public bool CanAddNewMultiple { get; set; }

        public int EntryCount { get; private set; }


        [DependsOn(nameof(MultipleInputsValid), nameof(MultipleOutputsValid),
            nameof(MultiplesContainDuplicates), nameof(EntryCount))]
        public bool CanGenerateOutput
        {
            get
            {
                bool value = MultipleInputsValid && MultipleOutputsValid
                 && EntryCount > 0 && !MultiplesContainDuplicates;

                return value;

            }
        }

        public BindingList<MultipleEntry> Multiples { get; private set; }




        public MultipleInputViewModel(IMessageService messageService, IMessageBus messageBus)
        {
            _messageService = messageService;
            _messageBus = messageBus;

            Multiple = "0";
            Value = "Enter new value";

            MultipleInputsValid = true;
            MultipleOutputsValid = true;
            MultiplesContainDuplicates = false;

            Multiples = new BindingList<MultipleEntry>();
            Multiples.RaiseListChangedEvents = true;
            Multiples.ListChanged += MultiplesOnListChanged;
        }

        private void MultiplesOnListChanged(object sender, ListChangedEventArgs e)
        {
            EntryCount = Multiples.Count;
        }


        public string ValidateMultipleInput(string input)
        {

            int value;

            if (!int.TryParse(input, out value))
            {
                MultipleInputsValid = false;
                return "please enter a valid whole number.";
            }

            MultipleInputsValid = true;
            return string.Empty;
        }

        public string ValidateValueInput(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                MultipleOutputsValid = false;
                return "output cannot be empty";
            }

            MultipleOutputsValid = true;
            return string.Empty;
        }



        public void InsertNew()
        {


            if (Multiples.Any(m => m.Multiple == Multiple))
            {
                _messageService.DisplayError("Input error",
                    "Multiple value already exists in input set. \nPlease enter a unique multiple.");
            }
            else
            {

                Multiples.Add(new MultipleEntry
                {
                    Multiple = Multiple,
                    Value = Value
                });
            }
        }

        public void GenerateOutput()
        {
            _messageBus.Publish(new GenerateOutputMessage
            {
                MultiplesSet = Multiples.ToDictionary(m => int.Parse(m.Multiple), m => m.Value)
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
