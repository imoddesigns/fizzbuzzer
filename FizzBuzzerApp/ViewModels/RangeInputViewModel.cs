﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzzerApp.Contracts;
using FizzBuzzerApp.Messages;
using PropertyChanged;

namespace FizzBuzzerApp.ViewModels
{
    [ImplementPropertyChanged]
    public class RangeInputViewModel
    {

        private readonly IMessageBus _messageBus;

        public string StartRange { get; set; }
        public string EndRange { get; set; }

        public bool RangeIsValid { get; set; }


        public RangeInputViewModel(IMessageBus messageBus)
        {
            _messageBus = messageBus;

            //set the default range
            StartRange = "1";
            EndRange = "100";

            RangeIsValid = true;

            messageBus.Publish(new UpdateRangeMessage
            {
                StartRange = int.Parse(StartRange),
                EndRange = int.Parse(EndRange)
            });
            
        }


        public string ValidateStartRange(string input)
        {
            int value;

            if (!int.TryParse(input, out value))
            {
                RangeIsValid = false;
                return "Please enter a valid whole number for start range";
            }

            RangeIsValid = true;

            return string.Empty;
        }

        public string ValidateEndRange(string input)
        {
            int value;

            if (!int.TryParse(input, out value))
            {
                RangeIsValid = false;
                return "please enter a valid whole number for start range";
            }

            if (value < int.Parse(StartRange))
            {
                RangeIsValid = false;
                return "end range must be greater than or equal to start range";
            }

            RangeIsValid = true;

            return string.Empty;
        }


        public void UpdateRange()
        {
            int startRange = int.Parse(StartRange);
            int endRange = int.Parse(EndRange);

            _messageBus.Publish(new UpdateRangeMessage
            {
                StartRange = startRange,
                EndRange = endRange
            });
        }
    }
}
