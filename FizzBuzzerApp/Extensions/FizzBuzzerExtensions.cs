﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FizzBuzzerApp.Extensions
{
   public static class FizzBuzzerExtensions
    {
        public static bool HandleTextBoxValidation(this UserControl control, Func<string, string> validator, TextBox tb, ErrorProvider ep, CancelEventArgs e)
        {
            var errorMessage = validator(tb.Text);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                ep.SetError(tb, errorMessage);
                ep.SetIconPadding(tb, -20);
                e.Cancel = true;
                return false;
            }

            ep.Clear();
            return true;
        }
    }
}
