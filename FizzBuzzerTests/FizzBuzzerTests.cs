﻿using System;
using System.Collections.Generic;
using FizzBuzzLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace FizzBuzzerTests
{
    [TestFixture]
    public class FizzBuzzerTests
    {
        private FizzBuzzLib.FizzBuzzer _fizzBuzzer;

        [OneTimeSetUp]
        public void Setup()
        {
            _fizzBuzzer = new FizzBuzzer();

        }

        [Test]
        public void TestGetFizzForMultiplesOf3AndGetBuzzForMultiplesOf5()
        {

            List<string> expectedOutput = new List<string>
            {
                "1",
                "2",
                "Fizz",
                "4",
                "Buzz"
            };

            List<string> output = new List<string>();

            _fizzBuzzer.FizzBuzz(1, 5, new Dictionary<int, string>()
            {
                {3, "Fizz"},
                {5, "Buzz"}
            }, s => output.Add(s));

            Assert.AreEqual(5, output.Count);

            for (int i = 0; i < expectedOutput.Count; i++)
            {
                Assert.AreEqual(expectedOutput[i], output[i]);
            }
        }


        [Test]
        public void TestGetFizzBuzzForMultiplesOf3And5()
        {

            List<string> output = new List<string>();

            string fizzBuzz = "FizzBuzz";

            _fizzBuzzer.FizzBuzz(1, 30, new Dictionary<int, string>()
            {
                {3, "Fizz"},
                {5, "Buzz"}
            }, s => output.Add(s));

            Assert.AreEqual(30, output.Count);

            Assert.AreEqual(output[14], fizzBuzz);
            Assert.AreEqual(output[29], fizzBuzz);


        }

        [Test]
        public void TestGetExpectedOutputForMultiplesOfInput()
        {
            List<string> output = new List<string>();

            string fizz = "Fizz";
            string buzz = "Buzz";
            string foo = "Foo";
            string bar = "Bar";

            var input = new Dictionary<int, string>()
            {
                {3, "Fizz"},
                {5, "Buzz"},
                {7, "Foo"},
                {9, "Bar"}
            };


            _fizzBuzzer.FizzBuzz(1, 30, input, s => output.Add(s));

            Assert.AreEqual(30, output.Count);

            for (int i = 1; i < 30; i++)
            {
                string expectedOutput = string.Empty;

                foreach (int key in input.Keys)
                {
                    if (i % key == 0)
                        expectedOutput += input[key];
                }

                expectedOutput = string.IsNullOrEmpty(expectedOutput) ? i.ToString() : expectedOutput;

                Assert.AreEqual(expectedOutput, output[i - 1]);
            }

            
        }
    

    [OneTimeTearDown]
        public void TearDown()
        {
            _fizzBuzzer = null;
        }
    }
}
