﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FizzBuzzLib
{
   public class FizzBuzzer
   {
       private bool _canceled = false;

        public void FizzBuzz(int rangeStart, int rangeEnd, Dictionary<int, string> outputSet ,
            Action<string> onWrite)
        {

            for (int i = rangeStart; i <= rangeEnd; i++)
            {

                string output = string.Empty;

                if (_canceled)
                {
                    rangeEnd = i;
                    return;
                }
               
                foreach (int key in outputSet.Keys)
                {
                    
                    if (i % key == 0)
                    {
                      
                        output += outputSet[key];
                    
                    }
                }

             
                if (string.IsNullOrEmpty(output))
                {
                    onWrite(i.ToString());
                }
                 else
                {
                    onWrite(output);
                }
                
            }
        }

       public void Cancel()
       {
           _canceled = true;
       }
    }
}
